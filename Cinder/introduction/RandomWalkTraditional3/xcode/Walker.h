//
//  Walker.h
//  Intro
//
//  Created by Matthew Campbell on 3/11/13.
//
//

#ifndef __Intro__Walker__
#define __Intro__Walker__

#include <iostream>

class Walker {

private:
    float x;
    float y;

public:
    Walker();
    Walker(float startX, float startY);
    void display() const;
    void step();
};

#endif /* defined(__Intro__Walker__) */
