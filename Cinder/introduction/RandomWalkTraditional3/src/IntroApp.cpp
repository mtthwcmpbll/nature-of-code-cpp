#include "cinder/app/AppBasic.h"
#include "cinder/gl/gl.h"

#include "Walker.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class IntroApp : public AppBasic {
private:
    Walker *walker;
public:
    void prepareSettings(Settings *settings);
	void setup();
	void mouseDown(MouseEvent event);	
	void update();
	void draw();
};

void IntroApp::prepareSettings(Settings *settings){
    settings->setWindowSize(640,360);
    settings->setFrameRate(60.0f);
}

void IntroApp::setup()
{
    walker = new Walker();
    
    // clear out the window with white only once
	gl::clear(Color(1.0f, 1.0f, 1.0f));
}

void IntroApp::mouseDown(MouseEvent event)
{
}

void IntroApp::update()
{
    walker->step();
}

void IntroApp::draw()
{
    walker->display();
}

CINDER_APP_BASIC(IntroApp, RendererGl)
