#include "cinder/app/AppBasic.h"
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class IntroApp : public AppBasic {
private:
    vector<float> randomCounts;
public:
    void prepareSettings(Settings *settings);
    void setup();
	void mouseDown( MouseEvent event );	
	void update();
	void draw();
};

void IntroApp::prepareSettings(Settings *settings){
    settings->setWindowSize(800, 200);
    settings->setFrameRate(60.0f);
}

void IntroApp::setup()
{
    for (int i=0; i < 20; i++) {
        randomCounts.push_back(0);
    }
}

void IntroApp::mouseDown( MouseEvent event )
{
}

void IntroApp::update()
{
    // Pick a random number and increase the count
    int index = (int)Rand::randFloat(randomCounts.size());
    randomCounts[index]++;
}

void IntroApp::draw()
{
	// clear out the window with white
	gl::clear(Color(1.0f, 1.0f, 1.0f));
    
    // Draw a rectangle to graph results
    
    //stroke(0);
    //strokeWeight(2);
    //fill(127);
    
    int w = getWindowWidth()/randomCounts.size();
    
    for (int x = 0; x < randomCounts.size(); x++) {
        Rectf r(x*w, getWindowHeight()-randomCounts[x], x*w+(w-1), getWindowHeight());
        gl::color(0.5f, 0.5f, 0.5f);
        gl::drawSolidRect(r);
        
        gl::color(0.0f, 0.0f, 0.0f);
        gl::drawStrokedRect(r);
    }
}

CINDER_APP_BASIC( IntroApp, RendererGl )
