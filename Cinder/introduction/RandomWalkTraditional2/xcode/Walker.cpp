//
//  Walker.cpp
//  Intro
//
//  Created by Matthew Campbell on 3/11/13.
//
//

#include "Walker.h"

#include "cinder/app/AppBasic.h"
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"

using namespace ci;
using namespace ci::app;
using namespace std;


Walker::Walker() {
    x = getWindowWidth() / 2;
    y = getWindowHeight() / 2;
}

Walker::Walker(int startX, int startY) {
    x = startX;
    y = startY;
}

void Walker::display() const {
    gl::color(0.0f, 0.0f, 0.0f);
    gl::drawSolidCircle(Vec2f(x, y), 1.0f);
}

void Walker::step() {
    int stepx = Rand::randInt(3)-1;
    int stepy = Rand::randInt(3)-1;
    x += stepx;
    y += stepy;
    x = constrain(x, 0, getWindowWidth()-1);
    y = constrain(y, 0, getWindowHeight()-1);
}