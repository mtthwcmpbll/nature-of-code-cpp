//
//  Walker.h
//  Intro
//
//  Created by Matthew Campbell on 3/11/13.
//
//

#ifndef __Intro__Walker__
#define __Intro__Walker__

#include <iostream>

class Walker {

private:
    int x;
    int y;

public:
    Walker();
    Walker(int startX, int startY);
    void display() const;
    void step();
};

#endif /* defined(__Intro__Walker__) */
